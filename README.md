# Kubernetes

## What is kubernetes?

## Why we use kubernetes?

## Kubernetes Architecture

## Kubernetes Objects:
- Nodes: Worker node
- Pods: Smallest deployable entity of k8s. Wrapper around container. Every pod has its own unique IP address
- Service: Expose application inside / outside of the cluster
    -   ClusterIP: Expose applciation inside the cluster. Used for inside communication only.
    -   NodePort: Expose application outside the cluster using Node Port. Can access application by NODEIP:NODEPORT
    -   LoadBalancer: Export application outside the cluster on LoadBalancer Service 
- Namespace: Devide the k8s cluster. Segrigate the resources.
- ReplicationController: Responsible for creating replicas of the pods. Used equality based selector. apiVersion: v1
- ReplicaSet: Responsible for creating replicas of the pods. Used set-based selector. Multiple label selector is possible. apiVersion: apps/v1. Set Operators: In, NotIn, Exist
- Deployment: Deployment is responsible for 




